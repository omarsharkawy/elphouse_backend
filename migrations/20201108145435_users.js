exports.up = function(knex) {
  return knex.schema.createTable('users', function (table) {
    table.increments('id').primary()
    table.string('name', 255)
    table.string('password', 255)
		table.string('mobile', 255)
		.notNullable()
		.unique()
		table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
		table.index(['mobile'])
  })
};

exports.down = function(knex) {
	return knex.schema.dropTableIfExists('users')
};
