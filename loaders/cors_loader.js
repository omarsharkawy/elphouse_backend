const cors = require('cors')

function load (app) {
  app.options('*', cors())
  app.use(cors())
}

module.exports = {
  load
}
