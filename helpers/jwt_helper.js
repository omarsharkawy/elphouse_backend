const jwt = require('jsonwebtoken')

function getTokenPayload (token, tokenKey) {
  const result = {}
  return new Promise(function (resolve, reject) {
    jwt.verify(token, tokenKey, function (err, payload) {
      if (err) {
        result.success = false
        result.errors = err
        reject(result)
      }
      if (payload) {
        result.success = true
        result.payload = payload
      }
      resolve(result)
    })
  })
}

module.exports = {
  getTokenPayload
}
