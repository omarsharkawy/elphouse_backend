module.exports = {
  swagger: "2.0",
  explorer: true,
  securityDefinitions: {
    name: 'bearer',
    schema: {
      type: 'apiKey',
      in: 'header',
      name: 'Authorization',
      description: ''
    },
    value: 'Bearer <my own JWT token>'
  },
  info: {
    version: "1.0.0",
    title: "Eplhouse Swagger",
    description: "backend of Eplhouse",
  },
  host: `${process.env.SWAGGER_HOST}`,
  basePath: "/",
  components: {
    securitySchemes: {
      bearer: {
        description: '',
        type: 'apiKey',
        name: 'Authorization',
        in: 'header'
      }
    }
  },
  security: {
    bearer: []
  },
  tags: [
    {
      name: 'Authentication',
      description: 'API for Authentication in the system'
    },
    {
      name: "Users",
      description: "API for Users in our system",
    },
  ],
  consumes: ["application/json"],
  produces: ["application/json"],
  paths: {
    "/api/users": {
      post: {
        tags: ["Users"],
        summary: "Create User",
        consumes: ["application/json"],
        parameters: [
          {
            in: "body",
            name: "Users",
            required: true,
            schema: {
              properties: {
								name: {
									type: "string",
								},
                mobile: {
                  type: "string",
								},
								password: {
									type: "string",
								}
              },
            },
          },
        ],
        responses: {
          200: {
            description: "OK",
          },
          400: {
            description: "Failed. Bad post data.",
          },
          404: {
            description: "Failed. Authentication failed.",
          },
          500: {
            description: "internal server error",
          },
        },
      },
      get: {
        tags: ["Users"],
				summary: "Get all Users",
				parameters: [
					{
						name: 'Authorization',
						in: 'header',
						description: 'an authorization header',
						required: true,
						type: 'string'
					},
        ],
        responses: {
          200: {
            description: "OK",
          },
          403: {
            description: "Please Log in/Unauthorized access",
          },
          500: {
            description: "internal server error",
          },
        },
			},
			put: {
				tags: ["Users"],
        summary: "Trasfer balance to User",
				consumes: ["application/json"],
        parameters: [
					{
						name: 'Authorization',
						in: 'header',
						description: 'an authorization header',
						required: true,
						type: 'string'
					},
          {
            in: "body",
            name: "Users",
            required: true,
            schema: {
              properties: {
								from_user_id:{
									type: "number"
								},
								to_user_id:{
									type: "number"
								},
								balance_value:{
									type: 'number'
								}
              },
            },
          },
        ],
        responses: {
          200: {
            description: "OK",
          },
          400: {
            description: "Failed. Bad post data.",
          },
          404: {
            description: "Failed. Authentication failed.",
          },
          500: {
            description: "internal server error",
          },
        },
			}
		},
		'/api/sessions': {
      post: {
        tags: ['Authentication'],
        summary: '(S101) Returning jwt token by logging in using email and password',
        consumes: ['application/json'],
        parameters: [{ in: 'body',
          name: 'Authentication',
          description: 'Authentication Object',
          required: true,
          schema: {
            properties: {
              mobile: {
                type: 'string'
              },
              password: {
                type: 'string'
              }
            }
          }
        }],
        responses: {
          200: {
            description: 'OK'
          },
          400: {
            description: 'Failed. Bad post data.'
          },
          404: {
            description: 'Failed. Authentication failed.'
          }
        }
      }
    },
		

    // "/api/specialities": {
    //   get: {
    //     tags: ["Specialities"],
    //     summary: "Get all Specialities",
    //     responses: {
    //       200: {
    //         description: "OK",
    //       },
    //       403: {
    //         description: "Please Log in/Unauthorized access",
    //       },
    //       500: {
    //         description: "internal server error",
    //       },
    //     },
    //   },
    //   post: {
    //     tags: ["Specialities"],
    //     summary: "Create Speciality",
    //     consumes: ["application/json"],
    //     parameters: [
    //       {
    //         in: "body",
    //         name: "Specialities",
    //         required: true,
    //         schema: {
    //           properties: {
    //             name: {
    //               type: "string",
    //             },
    //           },
    //         },
    //       },
    //     ],
    //     responses: {
    //       200: {
    //         description: "OK",
    //       },
    //       400: {
    //         description: "Failed. Bad post data.",
    //       },
    //       404: {
    //         description: "Failed. Authentication failed.",
    //       },
    //       500: {
    //         description: "internal server error",
    //       },
    //     },
    //   },
    // },
    // "/api/doctors-specialities": {
    //   get: {
    //     tags: ["Doctors Specialities"],
    //     summary: "Get all Doctors",
    //     responses: {
    //       200: {
    //         description: "OK",
    //       },
    //       403: {
    //         description: "Please Log in/Unauthorized access",
    //       },
    //       500: {
    //         description: "internal server error",
    //       },
    //     },
    //   },
    // },
  },

  definitions: {},
};
