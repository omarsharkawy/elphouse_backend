/* eslint-disable camelcase */
const auth_service = require('../services/auth_service')

const login = async function (req, res, next) {
  const valid_request = req.body && req.body.mobile && req.body.password
  if (!valid_request) {
    res.status(422).json({ message: 'missing mobile or password' })
    return
  }
  const mobile = req.body.mobile
  const password = req.body.password
  try {
    const result = await auth_service.login(mobile, password)
    if (result.success) {
      var user = result.user
      res.json({
        id: user.id,
        mobile: user.mobile,
        balance: user.balance,
        name: user.name,
        token: `Bearer ${result.token}`
      })
    } else {
      res.status(422).json({ message: 'login failed', errors: result.errors })
    }
  } catch (e) {
    next(e)
  }
}

module.exports = {
  login
}
