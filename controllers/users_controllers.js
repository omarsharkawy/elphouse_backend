const UserService = require('../services/user_services/user_service')


const createUser = async function (req, res, next) {
  try {
    const valid_req = req.body
    if (!valid_req) {
      res.status(400).json({ status: 'Failure', message: 'params are missing' })
    }

		const CreationResult = await UserService.createUser(req.body)
    if (CreationResult.success === false) {
      res.status(400).json({
        status: 'Failure',
        message: 'could not signup',
      })
      return
    }

    return res.status(200).json({
      status: 'success',
      new_user: CreationResult.new_user,
      token: `Bearer ${CreationResult.result.token}`
    })
  } catch (e) {
    return next(e)
  }
}

const getUsers = async function (req, res, next){
	try{
		const getUsers = await UserService.getUsers()
		res.status(getUsers.status).json({
			users: getUsers.users
		})
	}
	catch(e)
	{
		next(e)
	}
}

const updateBalance = async function (req, res, next){
	try{
		const UpdateBalanceResponse = await UserService.updateBalance(req.body)
		res.status(UpdateBalanceResponse.status).json({
			status: UpdateBalanceResponse.statusString,
			message: UpdateBalanceResponse.message
		})
	}
	catch(e)
	{
		next(e)
	}
}
const getBalance = async function (req, res, next){
	try{
		const getBalance = await UserService.getBalance(req.body)
		res.status(getBalance.status).json({
			balance: getBalance.balance
		})
	}
	catch(e)
	{
		next(e)
	}
}
module.exports = {
	updateBalance,
	createUser,
	getUsers,
	getBalance
}
