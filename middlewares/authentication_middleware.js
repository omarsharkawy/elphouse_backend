const { getTokenPayload } = require('../helpers/jwt_helper')
const UserModel = require('../models/user')

async function authenticateReq (req, res, next) {
  const auhtorization_header = req.headers.authorization
  if (!validAuthHeader(auhtorization_header)) {
    res
      .status(400)
      .json({ status: 'Failure', message: 'authorization headers not present' })
    return
  }
  const token = extractTokenFromAuthHeader(auhtorization_header)
  const token_key = process.env.SECRET
  let jwt_verify_result = null
  try {
    jwt_verify_result = await getTokenPayload(token, token_key)
  } catch (e) {
    if (e.errors.name === 'JsonWebTokenError') {
      res.status(400).json(
        {
          status: 'Failure', message: 'wrong jwt'
        })
      return
    }
    next(e)
  }
  if (!jwt_verify_result.success) {
    res.status(400).json({ status: 'Failure', message: 'non valid jwt token' })
    return
  }
  const jwt_payload = jwt_verify_result.payload
	const user_id = jwt_payload.user_id
  const user = await UserModel.findUserById(user_id)
  if (!user) {
    res
      .status(400)
      .json({
        status: 'Failure',
        message: 'User not found for this jwt user id payload'
      })
    return
  }
  req.user = user
	req.jwt_payload = jwt_payload
	
  next()
}

function validAuthHeader (auhtorization_header) {
  return (
    auhtorization_header != null &&
    auhtorization_header.split(' ')[0] === 'Bearer'
  )
}

function extractTokenFromAuthHeader (header) {
  return header.split(' ')[1]
}
module.exports = {
  authenticateReq
}
