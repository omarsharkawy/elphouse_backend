const jwt = require('jsonwebtoken')
const generateJWT = async function (payload, secret, options) {
  return new Promise(function (resolve) {
    jwt.sign(payload, secret, options, (err, token) => {
      if (err) {
        throw err
      }
      resolve(token)
    })
  })
}
module.exports = {
  generateJWT
}
