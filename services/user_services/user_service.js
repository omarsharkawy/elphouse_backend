const UserModel = require('../../models/user')
const { generateJWT } = require('../authentication_services/jwt_service')
const Encrypt_Password = require('../../helpers/encryption_helper')


async function createUser (user) {
  const result = {}
  const new_user  = user
  const enc_helper = await Encrypt_Password.EncryptPassword(
    new_user.password
  )
  new_user.password = enc_helper

	const checkMobileExistence = await UserModel.getUserByMobile(new_user.mobile)
  if (checkMobileExistence.length === 0) {
    const new_user_object = await UserModel.createUser(new_user)
    const user = await UserModel.findUserByMobile(new_user.mobile)
    const jwt_data = { user_id: user.id, mobile: user.mobile }
    const jwt_token = await generateJWT(jwt_data, process.env.SECRET, {})
    result.user = user
    result.token = jwt_token
    return { new_user:  new_user_object , success: true, result }
  } else {
    return { success: false, message: 'Mobile Already Registered' }
  }
}

async function updateBalance(body){
	console.log("body: ", body)
	const userFromBalance = await UserModel.getUserBalance(body.from_user_id)
	if(userFromBalance[0].balance< body.balance){
		return {status: 400, statusString: "error", message: "no sufficient balance"}
	}
	const userToBalance = await UserModel.getUserBalance(body.to_user_id)
	
	const toUsernewBalance = body.balance_value+userToBalance[0].balance
	const updateToBalance = await UserModel.updateBalance(toUsernewBalance, body.to_user_id);
	const fromUsernewBalance = userFromBalance[0].balance-body.balance_value
	const updateFromBalance = await UserModel.updateBalance(fromUsernewBalance, body.from_user_id);
	return {status: 200, statusString: "sucess", message: "balance upadated"}
}

async function getUsers(){
	const getUsers = await UserModel.getUsers()
	return {status: 200, users: getUsers}
}

async function getBalance(body){
	const getUserBalance = await UserModel.getUserBalance(body.user_id)
	return {status: 200, balance: getUserBalance[0].balance}
}

module.exports = {
	updateBalance,
	createUser,
	getUsers,
	getBalance
}
