'use strict'
const User = require('../models/user')
const { checkEncryptedEqualVal } = require('../helpers/encryption_helper')
const { generateJWT } = require('./authentication_services/jwt_service')

module.exports.login = async function (mobile, password) {
  const result = {}
  const errors = {}

  const user = await User.query()
		.findOne({ mobile })
		
  if (!user) {
    errors.mobile = 'No mobile found'
    result.success = false
    result.errors = errors
    return result
  }
  const success = await checkEncryptedEqualVal(password, user.password)
  if (!success) {
    result.success = false
    errors.password = "Password doesn't match"
    result.errors = errors
    return result
  }

  const jwt_data = { user_id: user.id, mobile: user.mobile }
  const jwt_token = await generateJWT(jwt_data, process.env.SECRET, {})
  result.success = true
  result.user = user
  result.token = jwt_token
  return result
}
