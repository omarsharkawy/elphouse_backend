/**
 * @jest-environment node
 */

const funcs = require('../funcs/escortFuncs')

let token
let loggedInUser
// let tokenUser;
beforeAll(async () => {
  loggedInUser = await funcs.loginUser('123456', 'Pro@yahoo.com')
  token = loggedInUser.data
})

test('Checking loggedin users agency is 1', async () => {
  expect(token.agency_id).toBe(1)
})
