const authRouter = require('./auth')
const usersRouter = require('./users')
const healthRouter = require('./health')
module.exports.init = function (app) {
  app.use('/api', healthRouter.router)
  app.use('/api', authRouter.router)
  app.use('/api', usersRouter.router)
}
