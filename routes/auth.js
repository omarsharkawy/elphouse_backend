const express = require('express')
const authController = require('../controllers/auth_controller')
const router = express.Router()

/* GET users listing. */
router.post('/sessions', authController.login)

module.exports = {
  router
}
