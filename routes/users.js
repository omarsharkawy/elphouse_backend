const express = require('express')
const usersControllers = require('../controllers/users_controllers')

const router = express.Router()
const { authenticateReq } = require('../middlewares/authentication_middleware')

router.post('/users', usersControllers.createUser)

router.put(
  '/users',
  // authenticateReq,
  usersControllers.updateBalance
)
router.get(
  '/users',
  // authenticateReq,
  usersControllers.getUsers
)
router.post(
  '/users/balance',
  // authenticateReq,
  usersControllers.getBalance
)
module.exports = {
  router
}
