'use strict'
// const CityModel = require('../models/city')
// const DistrictModel = require('../models/district')

const Model = require('objection').Model

class User extends Model {
  // Table name is the only required property.
  static get tableName () {
    return 'users'
  }

//   static findUserByEmailAndPassword (email, password) {
//     return User.query().findOne({ email, password })
//   }

  static findUserById (id) {
    return User.query().findOne({ id })
  }

//   static findNotDeletedUser (id) {
//     if (id == null || typeof id !== 'number') {
//       throw new Error('can;t all findUserById with null id')
//     }
//     return User.query()
//       .select('id').where('id', '=', id).andWhere('deleted', '=', false).orWhere('deleted', '=', null)
//   }

  static createUser (user) {
    return User.query().insert({ name: user.name, mobile: user.mobile, password: user.password, balance: 1000 })
  }

//   static async createNewAgencyAdmin (userParams) {
//     return User.query().insert({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       image_path: userParams.image_path,
//       nationality_id: userParams.nationality_id,
//       deleted: false
//     })
//   }

//   static async createNewUser (userParams) {
//     return User.query().insert({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       image_path: userParams.image_path,
//       nationality_id: userParams.nationality_id,
//       deleted: false
//     })
//   }

//   static async UpdateNewUserSignUp (userParams, id) {
//     return User.query().update({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       image_path: userParams.image_path,
//       deleted: false,
//       nationality_id: userParams.nationality_id,
//       activated: true
//     }).where('id', '=', id)
//   }

//   static async createNewGlobalAdmin (userParams) {
//     return User.query().insert({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       deleted: false
//     })
//   }

//   static async createProductionEmployee (userParams) {
//     return User.query().insert({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       nationality_id: userParams.nationality_id,
//       activated: true,
//       deleted: false
//     })
//   }

//   signUpEmployee (userParams) {
//     return this.$query().patchAndFetch({
//       email: userParams.email,
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       password: userParams.password,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       address: userParams.address,
//       nationality_id: userParams.nationality_id,
//       activated: true
//     })
//   }

//   addRolesAndPermissions (agency_id, roles, permissions) {
//     roles = roles || []
//     permissions = permissions || []
//     const roles_permissions = roles.concat(permissions).map(x => { return { id: x.id, agency_id: agency_id } })
//     return this.$relatedQuery('roles').relate(roles_permissions)
//   }

//   static createEmptyUser (email) {
//     return User.query().insertAndFetch({
//       email: email,
//       activated: false
//     })
//   }

  static findUserByMobile (mobile) {
    return User.query().findOne({ mobile: mobile })
  }

  static getUserByMobile (mobile) {
    return User.query().select('*').where('mobile', '=', mobile)
	}
	static getUserBalance(user_id){
		return User.query().select('balance').where('id', '=', user_id)
	}
	static updateBalance(balance, to_user_id){
		return User.query().update({balance : balance}).where('id','=',to_user_id)
	}
	static getUsers () {
    return User.query().select('*').where('mobile', '!=', "admin")
	}

//   static updateResetColumn (userparams) {
//     return User.query()
//       .update({ reset: userparams.reset })
//       .where('id', '=', userparams.user_id)
//   }

//   static updatePassword (userparams) {
//     return User.query()
//       .update({ password: userparams.password })
//       .where('id', '=', userparams.user_id)
//   }

//   static GetAllUsers () {
//     return User.query().select('*')
//   }

//   static deleteEmployee (userparams) {
//     return User.query().update({ deleted: true }).where('id', '=', userparams)
//   }

//   static deleteTourGuide (userparams) {
//     return User.query().update({ deleted: true }).where('id', '=', userparams)
//   }

//   static async updateUser (userParams, user_id) {
//     return User.query().update({
//       first_name: userParams.first_name,
//       last_name: userParams.last_name,
//       address: userParams.address,
//       email: userParams.email,
//       city_id: userParams.city_id,
//       district_id: userParams.district_id,
//       image_path: userParams.image_path,
//       nationality_id: userParams.nationality_id
//     }).where('id', '=', user_id)
//   }

//   static async updateTourOperator (user_id, password, email) {
//     return User.query().update({
//       email: email,
//       password: password,
//       activated: true,
//       deleted: false
//     }).where('id', '=', user_id)
//   }

//   static get relationMappings () {
//     // importing user model here to avoid require loop
//     const Role = require('./role')
//     const Invitation = require('./invitation')
//     const MobileNumber = require('./mobile_number')
//     const nationality = require('./nationality')

//     return {
//       roles: {
//         relation: Model.ManyToManyRelation,
//         modelClass: Role,
//         filter: query => query.select('roles.id', 'roles.name', 'agency_id').where('users_roles.deleted', '=', false),
//         join: {
//           from: 'roles.id',
//           // ManyToMany relation needs the `through` object
//           // to describe the join table.
//           through: {
//             // If you have a model class for the join table
//             // you need to specify it like this:
//             // modelClass: PersonMovie,
//             from: 'users_roles.user_id',
//             to: 'users_roles.role_id',
//             extra: ['agency_id']
//           },
//           to: 'users.id'
//         }
//       },
//       invitation: {
//         relation: Model.HasOneRelation,
//         modelClass: Invitation,
//         join: {
//           from: 'users.id',
//           to: 'invitations.creator_id'
//         }
//       },
//       mobile_numbers: {
//         relation: Model.HasManyRelation,
//         modelClass: MobileNumber,
//         join: {
//           from: 'users.id',
//           to: 'mobile_numbers.user_id'
//         }
//       },
//       Nationality: {
//         relation: Model.HasOneRelation,
//         modelClass: nationality,
//         join: {
//           from: 'users.nationality_id',
//           to: 'nationalities.id'
//         }

//       }
//     }
//   }
}

module.exports = User
