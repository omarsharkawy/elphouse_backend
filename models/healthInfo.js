'use strict'

const Model = require('objection').Model

class health_information extends Model {
  // Table name is the only required property.
  static get tableName () {
    return 'health_information'
  }

  static gethealth_information () {
    return health_information.query().select('id', 'name')
  }
}

module.exports = health_information
