module.exports = {
  apps: [
    {
      name: 'Tourism Portal Backend',
      script: 'bin/www',
      node_args: '-r dotenv/config',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      },
      env_staging: {
        NODE_ENV: 'production'
      }
    }
  ]
}
